package com.tw;

public class MultiplicationTable {
    public String buildMultiplicationTable(int start, int end) {
        if (!isValid(start, end)) return null;
        return generateTable(start, end);
    }

    public Boolean isValid(int start, int end) {
        return isInRange(start) && isInRange(end) && isStartNotBiggerThanEnd(start, end);
    }

    public Boolean isInRange(int number) {
        return number >=1 && number <= 1000;
    }

    public Boolean isStartNotBiggerThanEnd(int start, int end) {
        return start <= end;
    }

    public String generateTable(int start, int end) {
        StringBuilder table = new StringBuilder();
        for (int row = start; row <= end; row++) {
//            table.append(generateLine(start, row)).append("\r\n");
            table.append(generateLine(start, row)).append(System.lineSeparator());
        }
        return table.toString().trim();
    }

    public String generateLine(int start, int row) {
        StringBuilder rowResult = new StringBuilder();
        for (int current = start; current <= row; current++) {
            rowResult.append(generateSingleExpression(current, row)).append("  ");
        }
        return rowResult.toString().trim();
    }

    public String generateSingleExpression(int multiplicand, int multiplier) {
        StringBuilder singleExpression = new StringBuilder();
        return String.format("%d*%d=%d", multiplicand, multiplier, multiplicand * multiplier);
//        return singleExpression.append(multiplicand).append("*").append(multiplier).append("=").append(multiplicand* multiplier).toString();
//        return multiplicand + "*" + multiplier + "=" + (multiplicand * multiplier);
    }
}
